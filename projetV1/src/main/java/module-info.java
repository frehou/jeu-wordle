module com.example.projetv1 {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.jsoup;


    opens com.example.projetv1 to javafx.fxml;
    exports com.example.projetv1;
}