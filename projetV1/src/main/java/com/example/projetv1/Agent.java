package com.example.projetv1;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class Agent {
    private static final String COULEUR_NOIR = "NOIR";
    private static final String COULEUR_ORANGE = "ORANGE";
    private static final String COULEUR_VERT = "VERT";

    // Méthode pour comparer le mot choisi aléatoirement avec le mot à deviner
    public static void comparerMots(String motADeviner, String motChoisi, List<Character> lettres, List<String> couleurs) {
        // Parcourir les lettres du mot choisi aléatoirement
        for (int i = 0; i < motChoisi.length(); i++) {
            char lettre = motChoisi.charAt(i);

            // Vérifier si la lettre est dans le mot à deviner et si elle est à la bonne position
            if (i < motADeviner.length() && lettre == motADeviner.charAt(i)) {
                lettres.add(lettre); // Ajouter la lettre
                couleurs.add(COULEUR_VERT); // Ajouter la couleur verte
            } else if (motADeviner.indexOf(lettre) != -1) {
                lettres.add(lettre); // Ajouter la lettre
                couleurs.add(COULEUR_ORANGE); // Ajouter la couleur orange
            } else {
                lettres.add(lettre); // Ajouter la lettre
                couleurs.add(COULEUR_NOIR); // Ajouter la couleur noire
            }
        }
    }

    // Méthode pour démarrer le jeu
    public static void demarrerJeu(String motADeviner, List<String> motsFiltres, TextField[] textFields, EventHandler<ActionEvent> onJeuTermine) {
        String motChoisi = "";
            if (motsFiltres.isEmpty()) {
                System.out.println("Le mot est : " + motChoisi);
               // break;
            }

            // Choisir un mot aléatoire de la liste
            Random random = new Random();
            motChoisi = motsFiltres.get(random.nextInt(motsFiltres.size()));

        String finalMotChoisi1 = motChoisi;
        Thread insererLettreThread = new Thread(() -> {
            int index = 0;
            for (TextField textField : textFields) {
                try {
                    int finalIndex = index;
                    System.out.println(finalIndex);
                    Platform.runLater(() -> {
                        // Insérez votre logique de modification de l'interface utilisateur ici
                        textField.setText(String.valueOf(finalMotChoisi1.charAt(finalIndex)));
                    });
                     index++;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // Une fois l'insertion terminée, déclencher l'événement de fin de jeu
            if (onJeuTermine != null) {
                onJeuTermine.handle(null);
            }
        });
        insererLettreThread.start();


            // Créer des listes pour stocker les lettres et les couleurs
            List<Character> lettres = new ArrayList<>();
            List<String> couleurs = new ArrayList<>();

            // Comparer le mot choisi aléatoirement avec le mot à deviner
            comparerMots(motADeviner, motChoisi, lettres, couleurs);

            // Afficher les résultats
            System.out.println("Le mot : " + motChoisi);
            for (int i = 0; i < lettres.size(); i++) {
                System.out.println("Lettre : " + lettres.get(i) + ", Couleur : " + couleurs.get(i));
            }
            if(motChoisi.equals(motADeviner)){
                System.out.println("Le mot a devine : " + motChoisi);
                return;
            }

            for (int i = 0; i < lettres.size(); i++) {
                if (couleurs.get(i).equals(COULEUR_VERT)) {
                    char lettreVerte = lettres.get(i);
                    ListIterator<String> iterator = motsFiltres.listIterator();
                    while (iterator.hasNext()) {
                        String mot = iterator.next();
                        if (mot.charAt(i) != lettreVerte) {
                            iterator.remove(); // Supprimer les mots qui n'ont pas la lettre verte à la même position
                        }
                        /*if (mot.equals(motChoisi)) {
                            iterator.remove(); // Supprimer le mot choisi lui-même
                        }*/
                    }
                }}
            for (int i = 0; i < lettres.size(); i++){
                if (couleurs.get(i).equals(COULEUR_ORANGE)) {
                    char lettreOrange = lettres.get(i);
                    ListIterator<String> iterator = motsFiltres.listIterator();
                    while (iterator.hasNext()) {
                        String mot = iterator.next();
                        if (mot.charAt(i) == lettreOrange && !mot.equals(motChoisi)) {
                            iterator.remove(); // Supprimer les mots qui ont la lettre orange à la même position
                        }
                        if (!mot.contains(String.valueOf(lettreOrange))) {
                            iterator.remove(); // Supprimer les mots qui ont la lettre orange à la même position
                        }
                    }
                }
            }
            for (int i = 0; i < lettres.size(); i++) {
                if (couleurs.get(i).equals(COULEUR_NOIR)) {
                    char lettreNoire = lettres.get(i);
                    ListIterator<String> iterator = motsFiltres.listIterator();
                    while (iterator.hasNext()) {
                        String mot = iterator.next();
                        if (mot.indexOf(lettreNoire) != -1 && !mot.equals(motChoisi)) {
                            iterator.remove(); // Supprimer les mots qui contiennent la lettre noire
                        }
                    }
                }
            }
            System.out.println("Mots filtrés après la mise à jour : " + motsFiltres);
    }
}
