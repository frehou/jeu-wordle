package com.example.projetv1;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Player {

	public String name;
	public String ip;
	public int score;
	public BufferedReader netIn;
	public PrintWriter netOut;

	public String list_mot;

	public String nombre_mot;

	public String duree_mot;

	public  String taille_mot;

	public Player(String name, String ip) {
		this.name = name;
		this.ip = ip;
		score = 0;
		netIn = null;
		netOut = null;
	}

	public Player(String name, String ip, String l_mots, String n_mots,String d_mot,String taille_mot) {
		this.name = name;
		this.ip = ip;
		score = 0;
		this.list_mot=l_mots;
		this.nombre_mot=n_mots;
		this.duree_mot=d_mot;
		this.taille_mot = taille_mot;
	}

	public void setList(String l_mots){
		this.list_mot =l_mots;
	}
	public void setNombre_mot(String n_mots){
		this.nombre_mot =n_mots;
	}

	public void setDuree_mot(String d_mots){
		this.duree_mot =d_mots;
	}
	public void setTaille_mot(String t_mots){
		this.taille_mot =t_mots;
	}
	public void addFlux(BufferedReader netIn, PrintWriter netOut) {
		this.netIn = netIn;
		this.netOut = netOut;
	}
}