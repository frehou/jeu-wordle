package com.example.projetv1;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;



public class SalonController {

    @FXML
    TextField nom,server,d_mot,n_mot,nom1,t_mot;
    public static Player p;

    Stage mainStage;
    List<String> listmots;

    @FXML
    VBox v_box_join,v_box_create;

    ParserMot parserMot = new ParserMot();

    public void setStage(Stage stage) {
        mainStage = stage;
    }


    @FXML
    public void showCreate() {
        v_box_create.setVisible(true);
        v_box_join.setVisible(false);
    }
    @FXML
    public void showJoin() {
        v_box_join.setVisible(true);
        v_box_create.setVisible(false);
    }

    @FXML
    public void clickToPlay() {
        if(!nom.getText().isEmpty() && !server.getText().isEmpty() && !d_mot.getText().isEmpty() && !n_mot.getText().isEmpty() && !t_mot.getText().isEmpty()){
            List<String> mots;
            int x = Integer.parseInt(t_mot.getText());
            listmots = parserMot.par(x);
            mots = parserMot.slectPlusMot(Integer.parseInt(n_mot.getText()),listmots,x);
            p = new Player(nom.getText(), server.getText(),mots.toString(),n_mot.getText(),d_mot.getText(),t_mot.getText());
            PlayerListner multi = new PlayerListner();
            multi.serverJoin(server.getText(), p, mainStage,n_mot.getText(),mots,d_mot.getText(),listmots,t_mot.getText());
        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ouups !");
            alert.setHeaderText("Vous devai remplire tous les champs.");

            alert.show();
        }
    }
    @FXML
    public void clickToJoin() {
        System.out.println("le nom : "+nom1.getText());
        System.out.println("le server : "+server.getText());

        if(!server.getText().isEmpty() && !nom1.getText().isEmpty()){
            p = new Player(nom1.getText(), server.getText());
            PlayerListner multi = new PlayerListner();
            multi.serverJoin2(server.getText(), p, mainStage);
        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ouups !");
            alert.setHeaderText("Vous devai remplire tous les champs.");
            alert.show();
        }
    }
}
