package com.example.projetv1;


import java.io.BufferedReader;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class ListenServerService extends Service<String> {

	@Override
	protected Task<String> createTask() {
		return new Task<String>() {
			@Override
			protected String call() throws Exception {
				BufferedReader netIn = PlayerListner.netIn;
				String msg = netIn.readLine();
				return msg;
			}

		};
	}

}
