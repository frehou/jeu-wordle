package com.example.projetv1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import javafx.concurrent.Service;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class PlayerListner {

	public static Player pl = SalonController.p;
	public static Server server;
	Player p1, p2;
	Stage primaryStage;
	String serverIP;

	ParserMot parserMot = new ParserMot();


	public static Socket commSocket;
	Alert alert;

	public static BufferedReader netIn;
	public PrintWriter netOut;

	List<String> listmots;




	public void serverJoin(String ip, Player p, Stage mainStage,String n_mot,List<String> l_mots,String d_mot,List<String> listmots,String taille_mot) {
		try {
			this.listmots=listmots;
			primaryStage = mainStage;
			serverIP = ip;
			commSocket = new Socket(serverIP, Reference.SOCKET);
			netIn = new BufferedReader(new InputStreamReader(commSocket.getInputStream()));
			netOut = new PrintWriter(commSocket.getOutputStream(), true);
			p.addFlux(netIn, netOut);
			System.out.println("premiiiierrr : ");
			netOut.println("addc-" + p.name + "-" + p.ip + "-" + l_mots + "-" + n_mot + "-" + d_mot + "-" + taille_mot);
			listen();

		} catch (IOException e) {
			alert = new Alert(AlertType.ERROR, "L'addresse IP est eronnée.", ButtonType.CLOSE);
			alert.setTitle(Reference.TITLE + "Erreur");
			alert.setHeaderText("Erreur");
			alert.show();
		}

	}

	public void serverJoin2(String ip, Player p, Stage mainStage) {
		try {
			primaryStage = mainStage;
			serverIP = ip;
			commSocket = new Socket(serverIP, Reference.SOCKET);
			netIn = new BufferedReader(new InputStreamReader(commSocket.getInputStream()));
			netOut = new PrintWriter(commSocket.getOutputStream(), true);
			p.addFlux(netIn, netOut);
			netOut.println("addj-" + p.name + "-" + p.ip);
			listen();

		} catch (IOException e) {
			alert = new Alert(AlertType.ERROR, "L'addresse IP est eronnée.", ButtonType.CLOSE);
			alert.setTitle(Reference.TITLE + "Erreur");
			alert.setHeaderText("Erreur");
			alert.show();
		}

	}

	public void listen() {
		System.out.println("deuxieeeemeee : ");
		Service<String> listen = new ListenServerService();
		listen.start();
		System.out.println("ERRRRRRRRRRR");
		listen.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent e) {
				System.out.println("troisieeeeeme : ");
				String msg[] = listen.getValue().split("-");
				 System.out.println(msg[0]);
				if (msg[0].equals("server")) {
					server = new Server(serverIP, msg[1], msg[2]);
					System.out.println("[LOG] Connect� au serveur '" + server.name + "' (" + server.ip + ") [Jeu : "
							+ server.game + "]");
					listen.restart();
				}
				if (msg[0].equals("player")) {
					System.out.println("[LOG] Joueur '" + msg[1] + "' (" + msg[2] + ") re�u");
					server.addPlayer(new Player(msg[1], msg[2],msg[3],msg[4],msg[5],msg[6]));
					if (!server.isFull){
						pl.setList(msg[3]);
						pl.setNombre_mot(msg[4]);
						pl.setDuree_mot(msg[5]);
						pl.setTaille_mot(msg[6]);
						server.addPlayer(pl);
					}
					System.out.println("22222222222");
					listen.restart();
				}
				if (msg[0].equals("start")) {
					server.isGameStarted = true;
					System.out.println("[LOG] Le jeu d�marre...");
					startGame(Integer.parseInt(msg[1]));
					if (alert != null)
						alert.close();
				}
				if (msg[0].equals("wait")) {
					server.addPlayer(pl);
					alert = new Alert(AlertType.INFORMATION, "En attente d'un adversaire... veuillez patienter !",
							ButtonType.CANCEL);
					alert.setTitle(Reference.TITLE + "Attente d'un adversaire");
					alert.setHeaderText("Attente d'un adversaire");
					System.out.println("[LOG] En attente d'un adversaire...");
					listen.restart();
					Optional<ButtonType> result = alert.showAndWait();
					if (result.isPresent() && result.get() == ButtonType.CANCEL && !server.isGameStarted) {
						try {
							commSocket = new Socket(serverIP, Reference.SOCKET);
							netOut = new PrintWriter(commSocket.getOutputStream(), true);
							netOut.println("remove-" + pl.name);
							commSocket.close();
							netIn.close();
							netOut.close();
						} catch (UnknownHostException ex) {
							ex.printStackTrace();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
						System.out.println("[LOG] Déconnecte du serveur");
					}
				}
			}
		});
	}

	public void startGame( int p) {
		server.setInfo(p);
		p1 = server.p1;
		p2 = server.p2;
		System.out.println("quatrieeeeemeee : " + server.p1.list_mot );
		System.out.println("quatrieeeeemeee : " + server.p2.list_mot );

		if(this.listmots == null){
			int x = Integer.parseInt(server.p1.taille_mot);
			this.listmots = parserMot.par(x);
		}

		String trimmed = server.p1.list_mot.substring(1, server.p1.list_mot.length() - 1);
		String[] elements = trimmed.split(", ");

		List<String> listmot = new ArrayList<>();
		for (String element : elements) {
			listmot.add(element);
		}
		try {
			if(Integer.parseInt(server.p1.taille_mot) == 7){
			FXMLLoader loader = new FXMLLoader(getClass().getResource("multiplayer-view-7.fxml"));
			VBox root = loader.load();
			Stage jeuStage = new Stage();
			Scene jeuScene = new Scene(root, 1080, 720);
			MultiplayerGame7 multiplayerGame = loader.getController();
			multiplayerGame.setStage(primaryStage,netIn,netOut,listmot,server.p1.nombre_mot,server.p1.duree_mot,listmots);
			jeuStage.setScene(jeuScene);
			jeuStage.setTitle("Serveur " + server.name + " (" + server.ip + ") - Wordle - " + pl.name);
			jeuStage.show();
			}
			if(Integer.parseInt(server.p1.taille_mot) == 6){
				FXMLLoader loader = new FXMLLoader(getClass().getResource("multiplayer-view-6.fxml"));
				VBox root = loader.load();
				Stage jeuStage = new Stage();
				Scene jeuScene = new Scene(root, 1080, 720);
				MultiplayerGame6 multiplayerGame = loader.getController();
				multiplayerGame.setStage(primaryStage,netIn,netOut,listmot,server.p1.nombre_mot,server.p1.duree_mot,listmots);
				jeuStage.setScene(jeuScene);
				jeuStage.setTitle("Serveur " + server.name + " (" + server.ip + ") - Wordle - " + pl.name);
				jeuStage.show();
			}
			if(Integer.parseInt(server.p1.taille_mot) == 5){
				FXMLLoader loader = new FXMLLoader(getClass().getResource("multiplayer-view.fxml"));
				VBox root = loader.load();
				Stage jeuStage = new Stage();
				Scene jeuScene = new Scene(root, 1080, 720);
				MultiplayerGame multiplayerGame = loader.getController();
				multiplayerGame.setStage(primaryStage,netIn,netOut,listmot,server.p1.nombre_mot,server.p1.duree_mot,listmots);
				jeuStage.setScene(jeuScene);
				jeuStage.setTitle("Serveur " + server.name + " (" + server.ip + ") - Wordle - " + pl.name);
				jeuStage.show();
			}
			} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
