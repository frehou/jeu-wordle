package com.example.projetv1;

import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;



public class ChoixJeuController {

    public static Player p;
    String pseudo;
    String ip;

    @FXML
    private Button jeu1Button;

    @FXML
    private Button jeu2Button;

    @FXML
    private Button jeu3Button;

    @FXML
    private Button retourButton;
    Stage mainStage;

    public void setStage(Stage stage) {
        mainStage = stage;
    }



    @FXML
    private void initialize() {
        jeu1Button.setOnAction(e -> lancerJeu(1));
        jeu2Button.setOnAction(e -> lancerJeu(2));
        jeu3Button.setOnAction(e -> lancerJeu(3));
        retourButton.setOnAction(e -> ((Stage) retourButton.getScene().getWindow()).close());
    }

    private void lancerJeu(int numeroJeu) {
        if (numeroJeu == 1) {
            System.out.println("Lancement jeu 1");
            afficherJeu();
        } else if (numeroJeu == 2){
            afficherJeu2();
        }else{
            System.out.println("Numéro de jeu non pris en charge");
        }
    }

    private void afficherJeu() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));
            VBox root = loader.load();
            HelloController controller = loader.getController();
            Stage jeuStage = new Stage();
            Scene jeuScene = new Scene(root, 1080, 720);
            jeuStage.setScene(jeuScene);

            jeuStage.setTitle("Jeu");
            jeuStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void afficherJeu2() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("SalonAccueil.fxml"));
            Parent pageJeuxRoot  = loader.load();
            Stage nouvelleStage = new Stage();
            Scene nouvelleScene = new Scene(pageJeuxRoot, 1080, 720);
            SalonController controller = (SalonController) loader.getController();
            controller.setStage(nouvelleStage);
            nouvelleStage.setTitle("Salon");
            nouvelleStage.setScene(nouvelleScene);
            nouvelleStage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }




    }


}

