package com.example.projetv1;

import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.fxml.Initializable;
import javafx.geometry.Point3D;

import java.io.*;
import java.net.URL;
import java.util.*;

import javafx.stage.Stage;
import javafx.util.Duration;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiplayerGame7  implements Initializable {
    // declare all Textfield , label and Button on our Interface
    @FXML
    Label resultLabel;
    @FXML
    TextField lettre1;
    @FXML
    TextField lettre2;
    @FXML
    TextField lettre3;
    @FXML
    TextField lettre4;
    @FXML
    TextField lettre5;
    @FXML
    TextField lettre6;
    @FXML
    TextField lettre7;
    @FXML
    TextField lettre8;
    @FXML
    TextField lettre9;
    @FXML
    TextField lettre10;
    @FXML
    TextField lettre11;
    @FXML
    TextField lettre12;
    @FXML
    TextField lettre13;
    @FXML
    TextField lettre14;
    @FXML
    TextField lettre15;
    @FXML
    TextField lettre16;
    @FXML
    TextField lettre17;
    @FXML
    TextField lettre18;
    @FXML
    TextField lettre19;
    @FXML
    TextField lettre20;
    @FXML
    TextField lettre51;
    @FXML
    TextField lettre101;
    @FXML
    TextField lettre151;
    @FXML
    TextField lettre201;
    @FXML
    TextField lettre511;
    @FXML
    TextField lettre1011;
    @FXML
    TextField lettre1511;
    @FXML
    TextField lettre2011;
    @FXML
    Button buttonA;
    @FXML
    Button buttonZ;
    @FXML
    Button buttonE;
    @FXML
    Button buttonR;
    @FXML
    Button buttonT;
    @FXML
    Button buttonY;
    @FXML
    Button buttonU;
    @FXML
    Button buttonI;
    @FXML
    Button buttonO;
    @FXML
    Button buttonP;
    @FXML
    Button buttonQ;
    @FXML
    Button buttonS;
    @FXML
    Button buttonD;
    @FXML
    Button buttonF;
    @FXML
    Button buttonG;
    @FXML
    Button buttonH;
    @FXML
    Button buttonJ;
    @FXML
    Button buttonK;
    @FXML
    Button buttonL;
    @FXML
    Button buttonM;
    @FXML
    Button buttonW;
    @FXML
    Button buttonX;
    @FXML
    Button buttonC;
    @FXML
    Button buttonV;
    @FXML
    Button buttonB;
    @FXML
    Button buttonN;
    @FXML
    Button buttonBAC;
    @FXML
    Button buttonAide;
    @FXML
    Button buttonRE;

    String mot;

    @FXML
    Label temps,name1,name2;

    Instant startTime;
    int seriactuelle = 0;
    int elapsedSeconds =0;
    MonMenus monMenus = new MonMenus();
    ParserMot parserMot = new ParserMot();
    Scores scores = new Scores();
    @FXML
    Label nom;

    Player p1, p2;
    Server server = PlayerListner.server;

    Stage primaryStage;

    PrintWriter netOut;
    BufferedReader netIn;

    Service<String> listen;

    Player pl = PlayerListner.pl;

    List<String> listmots;

    int p=0;

    int g=0;

    Timeline timeline;

    String duree_mot ;

    List<String> tousmots;



    public void setStage(Stage stage, BufferedReader netIn, PrintWriter netOut,List<String> mots,String n_mot,String d_mot,List<String> tousmots) {
        g=Integer.parseInt(n_mot)-1;
        duree_mot = d_mot;
        primaryStage = stage;
        this.netIn = netIn;
        this.netOut = netOut;
        listmots = mots;
        mot =listmots.get(p);
        this.tousmots = tousmots;
        System.out.println("le mot est  :  " + mot);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) throws NullPointerException {
        p1 = server.p1;
        p2 = server.p2;
        listen();

        init();
        // Platform.runLater(() -> {
        if (startTime == null) {
            startTime = Instant.now(); // Set the start time only if it's not set
        }
        creeALF();
        addAutoFocus(lettre1, lettre2, null);
        addAutoFocus(lettre2, lettre3, lettre1);
        addAutoFocus(lettre3, lettre4, lettre2);
        addAutoFocus(lettre4, lettre5, lettre3);
        addAutoFocus(lettre5, lettre51, lettre4);
        addAutoFocus(lettre51, lettre511, lettre5);
        addAutoFocus(lettre511, null, lettre51);

        addAutoFocus(lettre6, lettre7, null);
        addAutoFocus(lettre7, lettre8, lettre6);
        addAutoFocus(lettre8, lettre9, lettre7);
        addAutoFocus(lettre9, lettre10, lettre8);
        addAutoFocus(lettre10, lettre101, lettre9);
        addAutoFocus(lettre101, lettre1011, lettre10);
        addAutoFocus(lettre1011, null, lettre101);

        addAutoFocus(lettre11, lettre12, null);
        addAutoFocus(lettre12, lettre13, lettre11);
        addAutoFocus(lettre13, lettre14, lettre12);
        addAutoFocus(lettre14, lettre15, lettre13);
        addAutoFocus(lettre15, lettre151, lettre14);
        addAutoFocus(lettre151, lettre1511, lettre15);
        addAutoFocus(lettre1511, null, lettre151);

        addAutoFocus(lettre16, lettre17, null);
        addAutoFocus(lettre17, lettre18, lettre16);
        addAutoFocus(lettre18, lettre19, lettre17);
        addAutoFocus(lettre19, lettre20, lettre18);
        addAutoFocus(lettre20, lettre201, lettre19);
        addAutoFocus(lettre201, lettre2011, lettre20);
        addAutoFocus(lettre2011, null, lettre201);


        startTimelines(temps);
        //});
    }

    public void init() {
        // Affichage
        // score1.setText(server.p1.score + "");
        //  score2.setText(server.p2.score + "");
        if (pl.name == server.p1.name){
            name1.setText(p1.name + " score 0");
            name1.setStyle("-fx-text-fill: red;");
            name2.setText(p2.name + " score 0");
        }else{
            name1.setText(p1.name + " score 0");
            name2.setText(p2.name + " score 0");
            name2.setStyle("-fx-text-fill: yellow;");
        }
    }

    /**
     * Gere les reponses du serveur
     */
    public void listen() {
        listen = new ListenServerService();
        listen.start();
        listen.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent e) {
                String msg[] = listen.getValue().split("-");
                if (msg[0].equals("partie")) {
                    if (msg[1].equals("1")) {
                        p1.score++;
                        resultLabel.setText("Le joueur  " + p1.name + " a gagne ! ");
                        name1.setText(p1.name + " score " + p1.score);
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Partie !");
                        alert.setHeaderText("Le joueur " + p1.name + " a gagne !");

                        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                        alert.getDialogPane().getStyleClass().add("positioned-alert");

                        // Affichage de l'alerte
                        alert.show();

                        // Création d'une transition de pause de 3 secondes
                        PauseTransition pause = new PauseTransition(Duration.seconds(3));
                        pause.setOnFinished(event -> {
                            // Actions à effectuer lorsque la transition est terminée (fermer l'alerte et recommencer le jeu)
                            alert.close();
                            resetGame();
                            lettre1.requestFocus();
                        });
                        pause.play();
                        // score1.setText(p1.score + "");
                    }
                    if (msg[1].equals("2")) {
                        p2.score++;
                        resultLabel.setText("Le joueur  " + p2.name + " a gagne !");
                        name2.setText(p2.name + " score " + p2.score);
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Partie !");
                        alert.setHeaderText("Le joueur  " + p2.name + " a gagne !");

                        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                        alert.getDialogPane().getStyleClass().add("positioned-alert");

                        // Affichage de l'alerte
                        alert.show();

                        // Création d'une transition de pause de 3 secondes
                        PauseTransition pause = new PauseTransition(Duration.seconds(3));
                        pause.setOnFinished(event -> {
                            // Actions à effectuer lorsque la transition est terminée (fermer l'alerte et recommencer le jeu)
                            alert.close();
                            resetGame();
                            lettre1.requestFocus();
                        });
                        pause.play();

                        //score2.setText(p2.score + "");
                    }
                    p++;
                    mot =listmots.get(p);
                }
                if (msg[0].equals("servupdate")){
                    if (msg[1].equals("1")){
                        p1.score++;
                    }
                    if (msg[1].equals("2")){
                        p2.score++;
                    }
                }
                if (msg[0].equals("end")){
                    if (msg[1].equals("1")){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("GameOver !");
                        alert.setHeaderText("Le joueur " + p1.name + " a gagne !");
                        alert.setContentText("Joueur " + p1.name + " score " + p1.score  +"\n" +
                                "Joueur " + p2.name + " score " + p2.score );

                        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                        alert.getDialogPane().getStyleClass().add("positioned-alert");

                        // Affichage de l'alerte
                        alert.show();
                    }
                    if (msg[1].equals("2")){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("GameOver !");
                        alert.setHeaderText("Le joueur " + p2.name + " a gagne !");
                        alert.setContentText("Joueur " + p2.name + " score " + p2.score  +"\n" +
                                "Joueur " + p1.name + " score " + p1.score );

                        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                        alert.getDialogPane().getStyleClass().add("positioned-alert");

                        // Affichage de l'alerte
                        alert.show();
                    }
                    if (msg[1].equals("3")){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("GameOver !");
                        alert.setHeaderText("Egaliter ");
                        alert.setContentText("Joueur " + p2.name + " score " + p2.score  +"\n" +
                                "Joueur " + p1.name + " score " + p1.score );

                        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
                        alert.getDialogPane().getStyleClass().add("positioned-alert");

                        // Affichage de l'alerte
                        alert.show();
                    }
                }
                listen.restart();
            }
        });
    }



    // handle the insert and the delete of lettre in the Textfield
    public void addAutoFocus(TextField current, TextField next, TextField previous) {
        current.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() == 1) {
                if (next != null) {
                    next.requestFocus(); // Move to the next text field
                }
            } else if (newValue.length() > 1) {
                current.setText(newValue.substring(0, 1)); // Limit input to one character
            }
        });

        current.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.BACK_SPACE && current.getText().isEmpty()) {
                if (previous != null) {
                    previous.requestFocus(); // Move back to the previous text field
                }
                event.consume();
            }
        });
    }
    /************************************************ timer *****************************************/
    // init the timer
    public void startTimelines(Label temps) {
        // Create a timeline that triggers every second
        timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateElapsedTime(temps);
                final Timeline[] countdownTimeline = new Timeline[1];
                if (elapsedSeconds == Integer.parseInt(duree_mot)) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Temps écoulé !");
                        alert.setHeaderText("Les "+ duree_mot +" secondes pour trouver le mot sont écoulées.");

                        AtomicInteger countDown = new AtomicInteger(5);
                        countdownTimeline[0] = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
                            if (countDown.get() > 1) {
                                int x = countDown.getAndDecrement()-1;
                                alert.setContentText(" Prochain mot dans  " + x + " secondes.");
                            } else {
                                countdownTimeline[0].stop();
                                alert.hide();
                                resetGame();
                                if(p<g){
                                    p++;
                                    mot =listmots.get(p);
                                    System.out.println("Egaliter !");
                                }else {
                                    if (pl.name == server.p1.name){
                                        if (p1.score > p2.score) send("gameover-" + 1);
                                        if(p1.score < p2.score) send("gameover-" + 2);
                                        if(p1.score == p2.score) send("gameover-" + 3);
                                        elapsedSeconds = Integer.parseInt(duree_mot)+1;
                                    }
                                    elapsedSeconds = Integer.parseInt(duree_mot)+1;
                                }
                            }
                        }));
                        countdownTimeline[0].setCycleCount(5);
                        countdownTimeline[0].play();

                        // Afficher l'alerte
                        alert.showAndWait();
                    });
                }

            }
        }));



        // Set the timeline to repeat indefinitely
        timeline.setCycleCount(Timeline.INDEFINITE);

        // Start the timeline
        timeline.play();
    }
    public void updateElapsedTime(Label temps) {
        if (elapsedSeconds != Integer.parseInt(duree_mot)+1) {
            elapsedSeconds++;
            // Update the label with the elapsed time
            temps.setText(elapsedSeconds + " seconds");
        }
    }
    public void initsecond(){
        this.elapsedSeconds=0;
    }


    // handle the insert of the letter with the Button
    public void creeALF(){
        List<Button> buttons = Arrays.asList(
                buttonA,buttonB,buttonC,buttonD,buttonE,buttonF,buttonG,buttonH,buttonI
                ,buttonJ,buttonK,buttonL,buttonM,buttonN,buttonO,buttonP,buttonQ,buttonR,
                buttonS,buttonT,buttonU,buttonV,buttonW,buttonX,buttonY,buttonZ
        );
        List<TextField> letterFields = Arrays.asList(
                lettre1, lettre2, lettre3, lettre4, lettre5,
                lettre6, lettre7, lettre8, lettre9, lettre10,
                lettre11, lettre12, lettre13, lettre14, lettre15,
                lettre16 , lettre17 , lettre18 , lettre19 , lettre20
        );

        // Iterate through buttons to add event handlers
        for (int i = 0; i < 26; i++) {
            char letter = (char) ('A' + i);
            buttons.get(i).setOnAction(event -> {
                // Find the first empty TextField and insert the letter
                for (TextField textField : letterFields) {
                    if (textField.getText().isEmpty()) {
                        textField.setText(String.valueOf(letter));
                        textField.requestFocus();
                        break;  // Exit the loop after inserting the letter
                    }
                }
            });
        }
    }
    // delete a letter with the button backspace
    @FXML
    public void backsp(){
        List<TextField> letterField = Arrays.asList(
                lettre1, lettre2, lettre3, lettre4, lettre5,
                lettre6, lettre7, lettre8, lettre9, lettre10,
                lettre11, lettre12, lettre13, lettre14, lettre15
        );
        for (int i = letterField.size() - 1; i >= 0; i--) {
            TextField textField = letterField.get(i);
            String text = textField.getText();
            if (!text.isEmpty()) {
                textField.setText(text.substring(0, text.length() - 1));
                break;  // Exit the loop after removing the character
            }
        }
    }

    public void switchToMain(ActionEvent event) throws IOException {
        monMenus.switchToMain(event);}

    public void rejouers(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Recommencer");
        alert.setHeaderText("Voulez-vous rejouer?");

        ButtonType buttonNewGame = new ButtonType("Recommencer");
        ButtonType buttonReturn = new ButtonType("Retour à la page");

        alert.getButtonTypes().setAll(buttonNewGame, buttonReturn);
        alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());

        alert.showAndWait().ifPresent(buttonType -> {
            if (buttonType == buttonNewGame) {
                System.out.println("eeeeeeeeeeee");
                scores.initsecond();
                scores.initseriactuelle();
                mot = parserMot.slectMot(5,listmots);
                // Call a method to reset the game (clear text fields)
                resetGame();
                lettre1.requestFocus();
            }
        });

    }
    // show help
    public void afficherAide(){ monMenus.afficherAide(); }
    public void afficherScore(){ monMenus.afficherScores(); }
    // show indice
    public void afficherIndice(){ monMenus.afficherIndi(mot); }

    @FXML
    public void passerAuRow511(){searchWord(lettre1, lettre2,lettre3, lettre4,lettre5,lettre51,lettre511,lettre6); }
    @FXML
    public void passerAuRow1011(){ searchWord(lettre6, lettre7,lettre8, lettre9,lettre10,lettre101,lettre1011,lettre11);}
    @FXML
    public void passerAuRow1511(){searchWord(lettre11, lettre12,lettre13, lettre14,lettre15,lettre151,lettre1511,lettre16);}
    @FXML
    public void passerAuRow2011(){searchWord(lettre16, lettre17,lettre18, lettre19,lettre20,lettre201,lettre2011,lettre201);}


    private void addRotateAnimation2(TextField textField) {
        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(1), textField);
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(1);
        rotateTransition.setAxis(new Point3D(1, 0, 0));
        rotateTransition.play();
    }

    //List<String> motsFiltres = parserMot.parplus("https://raw.githubusercontent.com/LouanBen/wordle-fr/main/mots.txt",motsFiltre);
    public void searchWord(TextField letter1, TextField letter2, TextField letter3, TextField letter4, TextField letter5, TextField letter6, TextField letter7, TextField letter8) {
        String inputWord = letter1.getText() + letter2.getText() + letter3.getText() + letter4.getText() + letter5.getText() + letter6.getText()+ letter7.getText();
        inputWord = inputWord.toUpperCase();
        System.out.println(inputWord);
        TextField[] tab= {letter1, letter2, letter3, letter4,letter5,letter6,letter7};
        if (inputWord.length() == 7) {
            devinerMot(mot,inputWord,tab,letter8,resultLabel,this.lettre1,this.lettre2011);
        }

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] != null && i < inputWord.length()) {
                addRotateAnimation2(tab[i]);
            }
        }


    }

    private void addRotateAnimation(TextField textField) {
        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(1), textField);
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(1);
        rotateTransition.setAxis(new Point3D(0, 1, 0));
        rotateTransition.play();
    }
    // fonction to guess the word
    public void devinerMot(String randomWord, String inpWord, TextField[] textFields, TextField lettre, Label resultLabel,TextField premierlettre,TextField dernierlettre) {
        if(tousmots.contains(inpWord)) {
            resultLabel.setText("entre un mot de 7 lettre");
            boolean x=true;
            char ora=' ';
            for (int i = 0; i < randomWord.length(); i++) {
                char lettreAttendue = randomWord.charAt(i);
                char lettreDevinee = (i < inpWord.length()) ? inpWord.charAt(i) : ' ';
                boolean lettreCorrecte = false;
                if (lettreDevinee == lettreAttendue) {
                    textFields[i].setStyle("-fx-background-color: green;-fx-background-radius: 15;");
                    addRotateAnimation(textFields[i]);
                    lettreCorrecte = true;
                } else if (randomWord.contains(String.valueOf(lettreDevinee))) {
                    for ( int j = 0; j < randomWord.length(); j++){
                        char lettreAttendues = randomWord.charAt(j);
                        char lettreDevinees = (j < inpWord.length()) ? inpWord.charAt(j) : ' ';
                        System.out.println("tttttt  " +textFields[i].getText().toUpperCase().charAt(0));
                        System.out.println("rrrrrr  " +lettreDevinees);
                        if (lettreDevinees == lettreAttendues && textFields[i].getText().toUpperCase().charAt(0) == lettreDevinees) {
                            System.out.println("aaaaa " +lettreDevinees);
                            x=false;
                            j = randomWord.length();
                        }
                    }
                    if(x){
                        if(ora!=textFields[i].getText().toUpperCase().charAt(0)) {
                            textFields[i].setStyle("-fx-background-color: ORANGE; -fx-background-radius: 15;");
                            ora=textFields[i].getText().toUpperCase().charAt(0);}
                    }
                }
                x=true;
            }
            //if he/she find the word
            if (inpWord.equals(randomWord)) {
                scores.augseriactuelle();

                File file = new File("scores.txt");
                if(file.exists()){
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader(file));
                        BufferedWriter writer = new BufferedWriter(new FileWriter("score.txt"));
                        String line,lines;
                        int numeroLigneCourante =1;
                        while ((line = reader.readLine()) != null) {
                            int indexOfSpace = line.indexOf(" ");
                            String extractedPart = line.substring(0, indexOfSpace);
                            System.out.println(extractedPart + " " + scores.getserieactuelle());
                            if(Integer.parseInt(extractedPart) < scores.getserieactuelle()){
                                System.out.println("hhhhhh");
                                reader.close();
                                reader = new BufferedReader(new FileReader(file));
                                while ((lines = reader.readLine()) != null) {
                                    System.out.println("jjjjjjj");
                                    if (numeroLigneCourante != 3) {
                                        writer.write(lines);
                                        writer.newLine();
                                    }
                                    numeroLigneCourante++;
                                }
                                writer.write(scores.getserieactuelle() + " temps : " + scores.getelapsedSeconds());
                                writer.newLine();
                                reader.close();
                                writer.close();
                                reader = new BufferedReader(new FileReader("score.txt"));
                                writer = new BufferedWriter(new FileWriter(file));
                                while ((line = reader.readLine()) != null) {
                                    System.out.println("lllllllll");
                                    writer.write(line);
                                    writer.newLine();
                                }
                            }
                        }
                        reader.close();
                        writer.close();
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }else {
                    FileWriter fileWriter = null; // Append to the file
                    try {
                        System.out.println("ffffffff");
                        fileWriter = new FileWriter("scores.txt", true);
                        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                        bufferedWriter.write(scores.getserieactuelle() + " temps : " + scores.getelapsedSeconds());
                        bufferedWriter.newLine();
                        bufferedWriter.close();
                        System.out.println("ggggggggggg");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                //}
                // Display an alert when the word is correctly guessed
                if(p<g){
                    if (pl.name == server.p1.name)
                        send("place-" + 1);
                    else
                        send("place-" + 2);
                    System.out.println("Bravo ! Vous avez deviné le mot.");
                }else {
                    if (pl.name == server.p1.name) send("update-" + 1);
                    else send("update-" + 2);
                    if (p1.score > p2.score) send("gameover-" + 1);
                    if(p1.score < p2.score) send("gameover-" + 2);
                    if(p1.score == p2.score) send("gameover-" + 3);
                }
            } else{
                /*****************************   gerer la derniere lettre **************************************/
                if(dernierlettre == lettre){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Ouups !");
                    alert.setHeaderText("Vous n'avez pas deviné le mot.");
                    alert.setContentText("Voulez-vous rejouer?");

                    ButtonType buttonNewGame = new ButtonType("Nouvelle partie");
                    ButtonType buttonReturn = new ButtonType("Retour à la page");

                    alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());

                    alert.getButtonTypes().setAll(buttonNewGame, buttonReturn);

                    alert.showAndWait().ifPresent(buttonType -> {
                        if (buttonType == buttonNewGame) {
                            // Call a method to reset the game (clear text fields)
                            mot =parserMot.slectMot(5,listmots);
                            resetGame();
                            premierlettre.requestFocus();
                        }
                    });
                    scores.initseriactuelle();
                }else lettre.requestFocus();
            }
        }else {
            resultLabel.setText("ce mot n'exsiste pas dans la list des mots");
        }
    }


    // to reset the Game
    public void resetGame() {
        lettre1.clear();
        lettre2.clear();
        lettre3.clear();
        lettre4.clear();
        lettre5.clear();
        lettre6.clear();
        lettre7.clear();
        lettre8.clear();
        lettre9.clear();
        lettre10.clear();
        lettre11.clear();
        lettre12.clear();
        lettre13.clear();
        lettre14.clear();
        lettre15.clear();
        lettre16.clear();
        lettre17.clear();
        lettre18.clear();
        lettre19.clear();
        lettre20.clear();
        lettre101.clear();
        lettre1011.clear();
        lettre201.clear();
        lettre2011.clear();
        lettre51.clear();
        lettre511.clear();
        lettre151.clear();
        lettre1511.clear();


        resetTextFieldBackground(lettre1);
        resetTextFieldBackground(lettre2);
        resetTextFieldBackground(lettre3);
        resetTextFieldBackground(lettre4);
        resetTextFieldBackground(lettre5);
        resetTextFieldBackground(lettre6);
        resetTextFieldBackground(lettre7);
        resetTextFieldBackground(lettre8);
        resetTextFieldBackground(lettre9);
        resetTextFieldBackground(lettre10);
        resetTextFieldBackground(lettre11);
        resetTextFieldBackground(lettre12);
        resetTextFieldBackground(lettre13);
        resetTextFieldBackground(lettre14);
        resetTextFieldBackground(lettre15);
        resetTextFieldBackground(lettre16);
        resetTextFieldBackground(lettre17);
        resetTextFieldBackground(lettre18);
        resetTextFieldBackground(lettre19);
        resetTextFieldBackground(lettre20);
        resetTextFieldBackground(lettre101);
        resetTextFieldBackground(lettre1011);
        resetTextFieldBackground(lettre151);
        resetTextFieldBackground(lettre1511);
        resetTextFieldBackground(lettre201);
        resetTextFieldBackground(lettre2011);
        resetTextFieldBackground(lettre51);
        resetTextFieldBackground(lettre511);

        // Reset any other game-related variables or timers
        initsecond();
        updateElapsedTime(temps);
    }
    public void resetTextFieldBackground(TextField textField) {
        // Reset the background color of the given text field to black
        textField.setStyle("-fx-background-color: gray; -fx-background-radius: 15;");
    }

    public void send(String msg) {
        netOut.println(msg);
    }


}
