package com.example.games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javafx.concurrent.Service;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import com.example.obj.ListenStringService1;
import com.example.obj.ListenStringService2;
import com.example.obj.RestartService;
import com.example.obj.Server;

public class Wordle {

	int[] cases;
	int win;
	int nb;
	int turn;
	Server server;

	PrintWriter netOut1, netOut2;
	BufferedReader netIn1, netIn2;
	
	Service<String> listen1, listen2, restart;

	public void start(Server s) {
		System.out.println("[LOG] Jeu 'Wordle' demarre");
		// Initialisation flux
		server = s;
		netOut1 = server.p1.netOut;
		netOut2 = server.p2.netOut;
		netIn1 = server.p1.netIn;
		netIn2 = server.p2.netIn;
		// Initialisation Service
		listen1 = new ListenStringService1();
		listen1.start();
		listen1.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
		@Override
		public void handle(WorkerStateEvent e) {
			String msg = listen1.getValue();
			recieve(msg);
				if(!msg.split("-")[0].equals("quit"))
					listen1.restart();
			}
		});
		listen2 = new ListenStringService2();
		listen2.start();
		listen2.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent e) {
				String msg = listen2.getValue();
				recieve(msg);
				if(!msg.split("-")[0].equals("quit"))
					listen2.restart();
			}
		});
		restart = new RestartService();
		restart.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent arg0) {
				init();
				restart.reset();
			}
		});
		init();
	}
	
	public void init(){
		// Initialisation jeu
		send("start-" + turn);
	}

	public void recieve(String m) {
		String[] msg = m.split("-");
		if (msg[0].equals("place")) {
			int c = Integer.parseInt(msg[1]);
			System.out.println("[LOG] Le joueur " + c + " a gagne un mot");
			if(c == 1) send("partie-" + c);
			if(c == 2) send("partie-" + c);
		}
		if (msg[0].equals("update")) {
			int c = Integer.parseInt(msg[1]);
			System.out.println("[LOG] Le joueur " + c + " a gagne un game");
			if(c == 1) send("servupdate-" + c);
			if(c == 2) send("servupdate-" + c);
		}
		if (msg[0].equals("gameover")) {
			int c = Integer.parseInt(msg[1]);
			System.out.println("[LOG] Le joueur " + c + " a gagne un game");
			if(c == 1) send("end-" + c);
			if(c == 2) send("end-" + c);
			if(c == 3) send("end-" + c);
		}
	}



	public void send(String msg) {
		netOut1.println(msg);
		netOut2.println(msg);
	}
}
