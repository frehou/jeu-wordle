package com.example.obj;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Player {

	public String name;
	public String ip;
	public int score;
	public BufferedReader netIn;
	public PrintWriter netOut;

	public String list_mot;

	public String nombre_mot;

	public String duree_mot;
	public String taille_mot;

	public Player(String name, String ip) {
		this.name = name;
		this.ip = ip;
		score = 0;
		netIn = null;
		netOut = null;
	}

	public Player(String name, String ip, BufferedReader netIn, PrintWriter netOut) {
		this.name = name;
		this.ip = ip;
		score = 0;
		this.netIn = netIn;
		this.netOut = netOut;
	}

	public Player(String name, String ip, BufferedReader netIn, PrintWriter netOut,String l_mots,String n_mot,String d_mot,String t_mot) {
		this.name = name;
		this.ip = ip;
		score = 0;
		this.netIn = netIn;
		this.netOut = netOut;
		this.list_mot=l_mots;
		this.nombre_mot=n_mot;
		this.duree_mot=d_mot;
		this.taille_mot=t_mot;
	}
}