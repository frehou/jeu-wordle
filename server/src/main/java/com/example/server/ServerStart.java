package com.example.server;

import java.io.IOException;
import java.net.InetAddress;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import com.example.obj.Server;

public class ServerStart {

    Stage primaryStage;
    String localip;
    String ip, name, game;




    public static Server server;

    @FXML
    TextField serverName, serverIP;

    @FXML
    RadioButton morpion;
    ToggleGroup g = new ToggleGroup();

    @FXML
    CheckBox restart;



    @FXML
    HBox sbox;




    public void setStage(Stage stage) {
        primaryStage = stage;
    }





    @FXML
    public void initialize() {
        try {
            localip = InetAddress.getLocalHost().getHostAddress();
        } catch (IOException e) {
            e.printStackTrace();
        }
        morpion.setSelected(true);
        morpion.setToggleGroup(g);
        serverIP.setDisable(true);
        serverIP.setText(localip);
        serverName.setText("Serveur");
    }

    public void serverStart(ActionEvent e) {
        ip = serverIP.getText();
        name = serverName.getText();
        game = "Wordle";

        if (!ip.equals("") && !name.equals("") && game != null) {
            server = new Server(ip, name, game);
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("ServerWindow.fxml"));
                Parent root = loader.load();
                ServerWindow controller = (ServerWindow) loader.getController();
                controller.getStage(primaryStage);
                Scene scene = new Scene(root, 800, 500);
                primaryStage.setTitle(Reference.TITLE + "Serveur '" + server.name + "' (" + server.ip + ")");
                primaryStage.setScene(scene);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } else {
            Alert alert = new Alert(AlertType.WARNING,
                    "Vous devez entrer toutes les informations avant de creer un serveur.", ButtonType.OK);
            alert.setTitle("Attention");
            alert.show();
        }
    }

}
